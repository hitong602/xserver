#include<stdio.h>
#include<stdlib.h>
#include "linklist.h"



void LinkedListInit(LinkListPtr this)
{
    NodePtr h= malloc(sizeof(Node));
    NodePtr t= malloc(sizeof(Node));

    h->key = 0;
    h->data = NULL;
    h->next = t;
    h->prev = NULL;

    t->key = 0;
    t->data = NULL;
    t->prev = h;
    t->next = NULL;

    this->head = h;
    this->tail = t;

    this->count = 0;

}

void LinkedListRelease(LinkListPtr this) {
    for (NodePtr np= this->head->next; np != this->tail; np = np->next) {
        free(np);
    }
    free(this->head);
    free(this->tail);
}

int LinkedListExistAsData(LinkListPtr this,char *data) {
    for (NodePtr np= this->head->next; np != this->tail; np = np->next) {
        if(np->data == data)
            return 1;
    }
    return 0;
}


int LinkedListIsEmpty(LinkListPtr this) {
    if(this->head->next == this->tail)
        return 1;
    return 0;
}

int LinkedListGetCount(LinkListPtr this) {
    return this->count;
}


void LinkedListAdd(LinkListPtr this,unsigned long key,char *data) {

    NodePtr np= malloc(sizeof(Node));
    np->data = data;
    np->key = key;
    np->next = this->tail;
    np->prev = this->tail->prev;

    this->tail->prev->next = np;
    this->tail->prev=np;
    this->count++;
}

char* LinkedListRemoveAsKey(LinkListPtr this,unsigned long key) {
     for (NodePtr np= this->head; np; np=np->next) {
        if(np != this->head && np !=this->head && np->key == key ) {

            np->prev->next = np->next;
            np->next->prev = np->prev;
            char *data = np->data;
            free (np);
            this->count--;

            return data;
        }
    }
    return NULL;
}
char * LinkedListRemoveAsData(LinkListPtr this,char *data) {
    for (NodePtr np= this->head->next; np != this->tail; np=np->next) {
        if(np->data == data ) {
            np->prev->next = np->next;
            np->next->prev = np->prev;
            char *d = np->data;
            free(np);
            this->count--;
            return d;
        }
    }
    return NULL;
}
NodePtr LinkedListGetFirst(LinkListPtr this) {
    if(LinkedListIsEmpty(this))
        return NULL;
    return this->head->next;

}

char * LinkedListPopFirst(LinkListPtr this) {
    for (NodePtr np= this->head->next; np != this->tail; np=np->next) {
        np->prev->next = np->next;
        np->next->prev = np->prev;
        char *d = np->data;
        free(np);
        this->count--;
        return d;

    }
    return NULL;

}

char * LinkedListGetDataAsKey(LinkListPtr this, unsigned long key) {
    for (NodePtr np= this->head; np; np=np->next) {
        if(np != this->head && np !=this->head && np->key == key ) {
            return np->data;
        }
    }
    return NULL;
}
