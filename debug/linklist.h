#ifndef __LINKEDLIST__
#define __LINKEDLIST__



typedef struct _Node  {
    unsigned long  key;
	char * data;
	struct _Node* next;
	struct _Node* prev;
}Node,*NodePtr;

typedef struct _LinkList {
    NodePtr  head;
    NodePtr  tail;
    int count;
} LinkList, * LinkListPtr;

void LinkedListInit(LinkListPtr this);
void LinkedListRelease(LinkListPtr this);
void LinkedListAdd(LinkListPtr this,unsigned long key,char *data);
char* LinkedListRemoveAsKey(LinkListPtr this,unsigned long key);
char * LinkedListRemoveAsData(LinkListPtr this,char *data);
char * LinkedListGetDataAsKey(LinkListPtr this, unsigned long key);
int LinkedListGetCount(LinkListPtr this);
int LinkedListIsEmpty(LinkListPtr this);
int LinkedListExistAsData(LinkListPtr this,char *data);
NodePtr LinkedListGetFirst(LinkListPtr this);
char * LinkedListPopFirst(LinkListPtr this);





#endif
