#include "dixstruct.h"
#include "extnsionst.h"
#include "extinit.h"
#include <X11/extensions/debugproto.h>
#include "eventconvert.h"

#include <stdio.h>
#include <assert.h>

#include "linklist.h"

#include "protocol-versions.h"

#define REPLY_BUF_SIZE 1024

//确定一个中间的指针，可以截获当前客户端发过来的所有请求
typedef int (*ProcFunctionPtr)(ClientPtr /*pClient */);

//用于　拦截请求，将需要拦截客户端的请求先发到xdebug 然后，xdebug再转发．这样可以记录当前request的值

typedef struct
{
    ProcFunctionPtr *originalVector; // ptr to client's proc vector before Record stuck its nose in 
    ProcFunctionPtr recordVector[256]; // proc vector with pointers for recorded requests redirected to the function RecordARequest
} LogClientPrivateRec, *LogClientPrivatePtr;

static DevPrivateKeyRec LogClientPrivateKeyRec; // 在这个结构中，可以记录一个链表，映射每个客户端对应的LogClientPrivateRec，

#define LogClientPrivateKey (&LogClientPrivateKeyRec)

/*  RecordClientPrivatePtr RecordClientPrivate(ClientPtr)
 *  gets the client private of the given client.  Syntactic sugar.
 */
#define LogClientPrivate(_pClient) (LogClientPrivatePtr) \
    dixLookupPrivate(&(_pClient)->devPrivates, LogClientPrivateKey)

typedef struct _DebugContext
{
    XID id;
    ClientPtr pXDebugHost; //接收调试信息的客户端
    ClientPtr pBufClient;    //client whose protocol is in replyBuffer 
    LinkListPtr debuggingClients;    //希望这些客户端的信息返回去，这是一个链表
    char bufCategory;     // reply buffer的分类，　每次传输的buffer是一类的．
    unsigned int continuedReply:1;      /* recording a reply that is split up? */
    int sequenceNumber;
    char replyBuffer[REPLY_BUF_SIZE]; /* buffered recorded protocol */
    int numBufBytes;                  /* number of bytes in replyBuffer */

    int inFlush;                      /*  are we inside RecordFlushReplyBuffer */
} DebugContext, *DebugContextPtr;

//存放多个注册的客户端
typedef struct _DebugClient
{
    XID processID; // 进程的ID  ,

} DebugClient, *DebugClientPtr;

static RESTYPE UDebugContext; /* internal resource type for debug contexts */

static LinkList allContexts; //所有的context链表

#define VERIFY_CONTEXT(_pContext, _contextid, _client)                          \
    {                                                                           \
        int rc = dixLookupResourceByType((void **)&(_pContext), _contextid,     \
                                         UDebugContext, _client, DixUseAccess); \
        if (rc != Success)                                                      \
            return rc;                                                          \
    }


static void UninstallHooks(DebugContextPtr pContext , XID clientProcessId );

#pragma region "Query Version"
//QueryVersion
static int _X_COLD ProcDebugQueryVersion(ClientPtr client)
{
    /* REQUEST(xRecordQueryVersionReq); */
    xDebugQueryVersionReply rep = {
        .type = X_Reply,
        .sequenceNumber = client->sequence,
        .length = 0,
        .majorVersion = SERVER_DEBUG_MAJOR_VERSION,
        .minorVersion = SERVER_DEBUG_MINOR_VERSION};

    REQUEST_SIZE_MATCH(xDebugQueryVersionReq);
    if (client->swapped)
    {
        swaps(&rep.sequenceNumber);
        swaps(&rep.majorVersion);
        swaps(&rep.minorVersion);
    }
    WriteToClient(client, sizeof(xDebugQueryVersionReply), &rep);
    return Success;
}
static int _X_COLD SProcDebugQueryVersion(ClientPtr client)
{
    REQUEST(xDebugQueryVersionReq);

    swaps(&stuff->length);
    REQUEST_SIZE_MATCH(xDebugQueryVersionReq);
    swaps(&stuff->majorVersion);
    swaps(&stuff->minorVersion);
    return ProcDebugQueryVersion(client);
}

#pragma endregion

#pragma region "logging ......"

static void FlushReplyBuffer(DebugContextPtr pContext, void *data1, int len1, void *data2, int len2)
{

    if (!pContext->pXDebugHost || pContext->pXDebugHost->clientGone || pContext->inFlush)
        return;

    ++pContext->inFlush;
    if (pContext->numBufBytes)
        WriteToClient(pContext->pXDebugHost, pContext->numBufBytes, pContext->replyBuffer);

    pContext->numBufBytes = 0;
    if (len1)
        WriteToClient(pContext->pXDebugHost, len1, data1);
    if (len2)
        WriteToClient(pContext->pXDebugHost, len2, data2);
    --pContext->inFlush;
}
static void LogAProtocolElement(DebugContextPtr pContext, ClientPtr pClient,int category,void *data,int datalen, int padlen, int futurelen)
{

    CARD32 serverTime = 0;
    int replylen;
    Bool debuggingClientSwapped = pContext->pXDebugHost->swapped;

    if (debuggingClientSwapped)
    {
        replylen = 0;
    }

    if (futurelen >= 0)
    {

        xDebugProbeContextReply *pRep = (xDebugProbeContextReply *)pContext->replyBuffer;
        if (pContext->pBufClient != pClient || pContext->bufCategory != category)
        {
            FlushReplyBuffer(pContext, NULL, 0, NULL, 0);
            pContext->pBufClient = pClient;
            pContext->bufCategory = category;

        }
        if (!pContext->numBufBytes)
        {
            serverTime = GetTimeInMillis();
            pRep->type = X_Reply;
            pRep->category = category;  
            pRep->sequenceNumber = pContext->sequenceNumber;
            pRep->length = 0;

            pRep->server_time = serverTime;
            pRep->pad3 = 0;
            pRep->pad4 = 0;

            pContext->numBufBytes = SIZEOF(xDebugProbeContextReply);
        }
        /* adjust reply length */

        replylen = pRep->length;
        //长度是4字节的整数倍
        replylen += bytes_to_int32(datalen) + bytes_to_int32(futurelen);

        pRep->length = replylen;
    }

    if (REPLY_BUF_SIZE - pContext->numBufBytes >= datalen)
    {
        if (datalen)
        {

            static char padBuffer[3]; /* as in FlushClient */

            memcpy(pContext->replyBuffer + pContext->numBufBytes,
                   data, datalen - padlen);
            pContext->numBufBytes += datalen - padlen;
            memcpy(pContext->replyBuffer + pContext->numBufBytes,
                   padBuffer, padlen);
            pContext->numBufBytes += padlen;
        }
    }
    else
    {
        FlushReplyBuffer(pContext, (void *)NULL, 0, (void *)data, datalen - padlen);
    }
}

//送事件，　这个都是服务器的设备产生的，或者
static void LogSendProtocolEvents(DebugClientPtr pDCAP, DebugContextPtr pContext, xEvent *pev, int count)
{
    int ev; /* event index */

    if (!pContext->pXDebugHost)
        return;

    for (ev = 0; ev < count; ev++, pev++)
    {

        xEvent swappedEvent;
        xEvent *pEvToRecord = pev;

        if (pContext->pXDebugHost->swapped)
        {
            (*EventSwapVector[pEvToRecord->u.u.type & 0177])(pEvToRecord, &swappedEvent);
            pEvToRecord = &swappedEvent;
        }

        LogAProtocolElement(pContext, NULL,XDebugFromServer,pEvToRecord, sizeof(xEvent), 0, 0);
        SetCriticalOutputPending();

    } /* end for each event */

} /* RecordADeviceEvent */

static void LogADeliveredEventOrError(CallbackListPtr *pcbl, void *nulldata, void *calldata)
{
    EventInfoRec *pei = (EventInfoRec *)calldata;
    DebugContextPtr pContext;
    DebugClientPtr pDCAP;

    ClientPtr pClient = pei->client;

    if (allContexts.count == 0)
        return ;

    for (NodePtr pn = LinkedListGetFirst(&allContexts); pn != allContexts.tail; pn = pn->next)
    {
        pContext = (DebugContextPtr)pn->data;
        pDCAP = (DebugClientPtr)LinkedListGetDataAsKey(pContext->debuggingClients, pClient->clientIds->pid);
        if (pDCAP)
        {
            int ev;
            xEvent *pev = pei->events;
            for (ev = 0; ev < pei->count; ev++, pev++)
            {
                xEvent swappedEvent;
                xEvent *pEvToRecord = pev;

                if (pClient->swapped)
                {
                    (*EventSwapVector[pev->u.u.type & 0177])(pev, &swappedEvent);
                    pEvToRecord = &swappedEvent;
                }
                LogAProtocolElement(pContext, pClient, XDebugFromServer,pEvToRecord, SIZEOF(xEvent), 0, 0);
            }
        }
    }
}

static void LogADeviceEvent(CallbackListPtr *pcbl, void *nulldata, void *calldata)
{
    return 0;
    DeviceEventInfoRec *pei = (DeviceEventInfoRec *)calldata;
     if (allContexts.count == 0)
        return ;

    NodePtr pn = LinkedListGetFirst(&allContexts);
    if(!pn) 
        return ;
    for (; pn != allContexts.tail; pn = pn->next)
    {
        DebugContextPtr pContext = (DebugContextPtr)pn->data;
        for (NodePtr pc = LinkedListGetFirst(pContext->debuggingClients); pc->next != NULL; pc = pc->next)
        {

            DebugClientPtr pDCAP = (DebugClientPtr)pc->data;
            int count;
            xEvent *xi_events = NULL;

            /* TODO check return values */
            if (IsMaster(pei->device))
            {
                xEvent *core_events;

                EventToCore(pei->event, &core_events, &count);
                LogSendProtocolEvents(pDCAP, pContext, core_events,
                                      count);
                free(core_events);
            }

            EventToXI(pei->event, &xi_events, &count);
            LogSendProtocolEvents(pDCAP, pContext, xi_events, count);
            free(xi_events);
        }
    }
}

static void LogAReply(CallbackListPtr *pcbl, void *nulldata, void *calldata)
{
    DebugContextPtr pContext;
    DebugClientPtr pDCAP;

     if(allContexts.count ==0 )
        return ;
    

    ReplyInfoRec *pri = (ReplyInfoRec *)calldata;
    ClientPtr pClient = pri->client;

    for (NodePtr pn = LinkedListGetFirst(&allContexts); pn != allContexts.tail; pn = pn->next)
    {
        pContext = (DebugContextPtr)pn->data;
        pDCAP = (DebugClientPtr)LinkedListGetDataAsKey(pContext->debuggingClients, pClient->clientIds->pid);
        if (pDCAP)
        {
            int majorop = pClient->majorOp;

            if (pContext->continuedReply)
            {
                LogAProtocolElement(pContext, pClient, XDebugFromServer,(void *)pri->replyData, pri->dataLenBytes, pri->padBytes, /* continuation */ -1);
                if (!pri->bytesRemaining)
                    pContext->continuedReply = 0;
            }
            else if (pri->startOfReply)
            {
                if (majorop <= 127)  {
                     LogAProtocolElement(pContext, pClient,XDebugFromServer,
                                    (void *)pri->replyData,
                                    pri->dataLenBytes, 0,
                                    pri->bytesRemaining);
                    if (pri->bytesRemaining)
                        pContext->continuedReply = 1;
                } else {  //extension  ,暂时不处理
                //    LogAProtocolElement(pContext, pClient,XDebugFromServer,
                //                     (void *)pri->replyData,
                //                     pri->dataLenBytes, 0,
                //                     pri->bytesRemaining);
                //     if (pri->bytesRemaining)
                //         pContext->continuedReply = 1;
                }

               
            }
        }
    }
}

static void LogABigRequest(DebugContextPtr pContext, ClientPtr client, xReq * stuff)
{
    CARD32 bigLength;
    int bytesLeft;

   
    /* record the request header */
    bytesLeft = client->req_len << 2;
    LogAProtocolElement(pContext, client,XDebugFromClient,(void *) stuff, SIZEOF(xReq), 0, bytesLeft);

    /* reinsert the extended length field that was squished out */
    bigLength = client->req_len + bytes_to_int32(sizeof(bigLength));
    if (client->swapped)
        swapl(&bigLength);
    LogAProtocolElement(pContext, client,XDebugFromClient,(void *) &bigLength, sizeof(bigLength), 0,
                           /* continuation */ -1);
    bytesLeft -= sizeof(bigLength);

    /* record the rest of the request after the length */
    LogAProtocolElement(pContext, client,XDebugFromClient,(void *) (stuff + 1), bytesLeft, 0,/* continuation */ -1);
}  

static int LogARequest(ClientPtr client)
{
    DebugContextPtr pContext;
    DebugClientPtr pDCAP;
    LogClientPrivatePtr pClientPriv;

    REQUEST(xReq);
    int majorop;
    majorop = stuff->reqType;

    NodePtr pn = LinkedListGetFirst(&allContexts);
    if(!pn) {
        return BadAccess;
    }


    for (; pn != allContexts.tail; pn = pn->next) {
        pContext = (DebugContextPtr)pn->data;
        pDCAP = (DebugClientPtr)LinkedListGetDataAsKey(pContext->debuggingClients, client->clientIds->pid);
        if(pDCAP) {
            if (majorop <= 127) {       /* core request */
                if (stuff->length == 0)
                    LogABigRequest(pContext, client, stuff);
                else
                    LogAProtocolElement(pContext, client,XDebugFromClient,(void *)stuff,client->req_len << 2, 0, 0);
            } else { //extension, 
                if (stuff->length == 0)
                    LogABigRequest(pContext, client, stuff);
                else
                    LogAProtocolElement(pContext, client,XDebugFromClient,(void *)stuff,client->req_len << 2, 0, 0);
            }
        }

    }

    //继续主流程的调用
    pClientPriv = LogClientPrivate(client);
    assert(pClientPriv);
    return (*pClientPriv->originalVector[majorop]) (client);
} 

static void LogFlushAllContexts(CallbackListPtr *pcbl, void *nulldata, void *calldata)
{
    NodePtr pn = LinkedListGetFirst(&allContexts);
    if (!pn)
        return;

    for (; pn->next != NULL; pn = pn->next)
    {
        DebugContextPtr pContext = (DebugContextPtr)pn->data;

        if (pContext->numBufBytes)
            FlushReplyBuffer(pContext, NULL, 0, NULL, 0);
    }
}

static void FreeContext(DebugContextPtr pContext) {

    assert(pContext);
    DebugClientPtr dc = (DebugClientPtr) LinkedListPopFirst(pContext->debuggingClients);
    while (dc) {
        UninstallHooks(pContext,dc->processID);
        free (dc);
        dc = (DebugClientPtr)LinkedListPopFirst(pContext->debuggingClients);
    }
    free(pContext);
}
static void FreeContextAsClient(ClientPtr pClient)
{
    DebugContextPtr pContext = (DebugContextPtr)LinkedListRemoveAsKey(&allContexts, pClient->clientAsMask);

    if (pContext)
        FreeContext(pContext);
}

#pragma endregion

#pragma region "Register"
static void UnregisterClient(DebugContextPtr pContext, XID clientProcessId)
{
    DebugClientPtr pDCAP = (DebugClientPtr)LinkedListRemoveAsKey(pContext->debuggingClients, clientProcessId);
    if (pDCAP) {
        UninstallHooks(pContext , clientProcessId);
        free(pDCAP);
    }
}

static void UninstallHooks(DebugContextPtr pContext , XID clientProcessId ) {
     for (int i = 0; i < MAXCLIENTS; i++) {
        ClientPtr pClient = clients[i];
        if (!pClient)
            break;
        if (pClient->clientIds->pid == clientProcessId && !LogClientPrivate(pClient)) {
            //恢复当前的数据
            LogClientPrivatePtr pClientPriv;

            //从客户端取出　在registerClient　时候，保存的请求数据链表　，然后把保存的数据　恢复过来．
            pClientPriv = LogClientPrivate(pClient);
            if (pClientPriv) {
                memcpy(pClientPriv->recordVector, pClientPriv->originalVector, sizeof(pClientPriv->recordVector));
            
                //释放 拦截用的中间数据
                pClient->requestVector = pClientPriv->originalVector;
                dixSetPrivate(&pClient->devPrivates,LogClientPrivateKey, NULL);
                free(pClientPriv);
            }
            
            break;
        }
    }
    if (LinkedListIsEmpty(pContext->debuggingClients) == 0) //没有待调试的客户端了，就unhook
    {
        DeleteCallback(&EventCallback, LogADeliveredEventOrError, NULL);
        DeleteCallback(&DeviceEventCallback, LogADeviceEvent, NULL);
        DeleteCallback(&ReplyCallback, LogAReply, NULL);
        DeleteCallback(&FlushCallback, LogFlushAllContexts, NULL);
        /* Alternate context flushing scheme: delete the line above
            * and call RemoveBlockAndWakeupHandlers here passing
            * RecordFlushAllContexts.  Is this any better?
            */
        /* Having deleted the callback, call it one last time. -gildea */
        LogFlushAllContexts(&FlushCallback, NULL, NULL);
    }

}

static int InstallHooks(DebugContextPtr pContext,XID clientProcessId) {
    //根据 pDCAP->processID 找到其对应的的clientPtr，来截获这个客户端的数据
    for (int i = 0; i < MAXCLIENTS; i++) {
        ClientPtr pClient = clients[i];
        if (!pClient)
            break;
        if (pClient->clientIds->pid == clientProcessId && !LogClientPrivate(pClient)) {

            LogClientPrivatePtr pClientPriv;  //分配一个拦截某个客户端所需要的数据结构．

            /* 分配一个 LogClientPrivateRec*/
            pClientPriv = (LogClientPrivatePtr)malloc(sizeof(LogClientPrivateRec));
            if (!pClientPriv)
                return BadAlloc;
            /* 把客户端原有的接收请求数据的数据结构　备份到　pClientPriv　中 
            　　保留原有的requestVector数据指针和数据
            　　　因为request 拦截后，还要调用　原始的接口让Xorg继续后续的操作
                pClientPriv = LogClientPrivate(client);
                (*pClientPriv->originalVector[majorop]) (client);
            */
            memcpy(pClientPriv->recordVector, pClient->requestVector, sizeof(pClientPriv->recordVector));
            pClientPriv->originalVector = pClient->requestVector;

            //在这个要求拦截数据的客户端中　，　设置一个key,value 存放它原来的接收请求的数据结构．
            dixSetPrivate(&pClient->devPrivates, LogClientPrivateKey, pClientPriv);
            
            //给这个客户端安排新的数据接口．
            pClient->requestVector = pClientPriv->recordVector;

            for (int j=0; j<255 ; j++) {  //全部都复制吧，所有的请求都想记录．
                pClient->requestVector[j] = LogARequest;  
            }

            break;
        }
    }

    if (LinkedListGetCount(pContext->debuggingClients) == 1)      //对所有的客户端　，这些事件回调　只增加一次
    { //至少有一个客户端了，　就增加一个hook
        if (!AddCallback(&EventCallback, LogADeliveredEventOrError, NULL))
            return BadAlloc;
        if (!AddCallback(&DeviceEventCallback, LogADeviceEvent, NULL))
            return BadAlloc;
        if (!AddCallback(&ReplyCallback, LogAReply, NULL))
            return BadAlloc;
        if (!AddCallback(&FlushCallback, LogFlushAllContexts, NULL))
            return BadAlloc;
    }

    return Success;
}

static int RegisterClient(DebugContextPtr pContext, ClientPtr client, xDebugRegisterClientReq *stuff)
{
    int err = 0;
    //查询这个 process id 是否注册了！！ {}

    DebugClientPtr pDCAP = (DebugClientPtr)malloc(sizeof(DebugClientPtr));
    if (!pDCAP)
    {
        err = BadAlloc;
        goto bailout;
    }
    pDCAP->processID = stuff->window; //进程的id
    LinkedListAdd(pContext->debuggingClients, pDCAP->processID, (char *)pDCAP);

    if (InstallHooks(pContext , pDCAP->processID ) !=Success ) {
        UninstallHooks(pContext , pDCAP->processID);
        LinkedListRemoveAsKey(pContext->debuggingClients,pDCAP->processID);
        free(pDCAP);

    }
 // IgnoreClient(client);

bailout:
    return err;
} /* RecordRegisterClients */

static int _X_COLD ProcDebugRegisterClient(ClientPtr client)
{
    DebugContextPtr pContext;

    REQUEST(xDebugRegisterClientReq);

    REQUEST_AT_LEAST_SIZE(xDebugRegisterClientReq);
    VERIFY_CONTEXT(pContext, stuff->context, client);

    return RegisterClient(pContext, client, stuff);
}
static int _X_COLD SProcDebugRegisterClient(ClientPtr client)
{
    REQUEST(xDebugRegisterClientReq);

    swaps(&stuff->length);
    REQUEST_AT_LEAST_SIZE(xDebugRegisterClientReq);

    return ProcDebugRegisterClient(client);
}

static int _X_COLD ProcDebugUnregisterClient(ClientPtr client)
{
    DebugContextPtr pContext;

    REQUEST(xDebugUnregisterClientReq);

    REQUEST_AT_LEAST_SIZE(xDebugUnregisterClientReq);
    VERIFY_CONTEXT(pContext, stuff->context, client);

    UnregisterClient(pContext, stuff->window);

    return Success;
}
static int _X_COLD SProcDebugUnregisterClient(ClientPtr client)
{
    REQUEST(xDebugUnregisterClientReq);

    swaps(&stuff->length);
    REQUEST_AT_LEAST_SIZE(xDebugUnregisterClientReq);
    swapl(&stuff->context);
    SwapRestL(stuff);
    return ProcDebugUnregisterClient(client);
}
#pragma endregion
#pragma region "Context option"
// CreateContext
static int _X_COLD ProcDebugCreateContext(ClientPtr client)
{

    REQUEST(xDebugCreateContextReq);
    DebugContextPtr pContext;
    // DebugContextPtr *ppNewAllContexts = NULL;
    int err = BadAlloc;

    REQUEST_AT_LEAST_SIZE(xDebugCreateContextReq);
    LEGAL_NEW_RESOURCE(stuff->context, client);

    pContext = (DebugContextPtr)malloc(sizeof(DebugContext));
    if (!pContext)
        goto bailout;

    pContext->id = stuff->context;
    pContext->numBufBytes = 0;
    pContext->inFlush = 0;
    pContext->pXDebugHost = NULL;
    pContext->pBufClient = NULL;
    pContext->continuedReply = 0;
    pContext->bufCategory = 0;


    pContext->debuggingClients = (LinkListPtr)malloc(sizeof(LinkList));
    LinkedListInit(pContext->debuggingClients);

    LinkedListAdd(&allContexts, client->clientAsMask, (char *)pContext);

    if (AddResource(pContext->id, UDebugContext, pContext))
    {
        //ppAllContexts[numContexts++] = pContext;
        return Success;
    }
    else
    {
        return BadAlloc;
    }
bailout:
    free(pContext);
    return err;
}
static int _X_COLD SProcDebugCreateContext(ClientPtr client)
{

    REQUEST(xDebugCreateContextReq);

    swaps(&stuff->length);
    REQUEST_AT_LEAST_SIZE(xDebugCreateContextReq);

    return ProcDebugCreateContext(client);
}

static int _X_COLD ProcDebugFreeContext(ClientPtr client)
{
    DebugContextPtr pContext;

    REQUEST(xDebugFreeContextReq);

    REQUEST_SIZE_MATCH(xDebugFreeContextReq);
    VERIFY_CONTEXT(pContext, stuff->context, client);

    FreeResource(stuff->context, RT_NONE); //会调用　DebugDeleteContext　??这个函数？？？
    return Success;
}
static int _X_COLD SProcDebugFreeContext(ClientPtr client)
{
    REQUEST(xDebugFreeContextReq);

    swaps(&stuff->length);
    REQUEST_SIZE_MATCH(xDebugFreeContextReq);
    swapl(&stuff->context);
    return ProcDebugFreeContext(client);
}

static int _X_COLD ProcDebugProbeContext(ClientPtr client)
{
    DebugContextPtr pContext;

    REQUEST(xDebugProbeContextReq);
    // DebugClientAndProtocolPtr pDCAP;

    REQUEST_SIZE_MATCH(xDebugProbeContextReq);
    VERIFY_CONTEXT(pContext, stuff->context, client);

    if (pContext->pXDebugHost)
        return BadMatch; /* already enabled */

    IgnoreClient(client);
    pContext->pXDebugHost = client; //因为数据 和 显示 的 connect 可能不一样，所以需要在这里赋值
    pContext->sequenceNumber = client->sequence;

    if (LinkedListIsEmpty(pContext->debuggingClients))
        return BadAccess;

    //   xDebugProbeContextReply rep = {
    //     .type = X_Reply,
    //     .sequenceNumber = client->sequence,
    //     .length = 0,
    //     .category = 0,
    //     .server_time=0

    // };

    // WriteToClient(pContext->pXDebugHost, SIZEOF(xDebugProbeContextReply), &rep);

    //printf(stdout,"ProcDebugProbeContext : %d", pContext->pXDebugHost);

    // /* send StartOfData */
    // LogAProtocolElement(pContext, NULL, NULL, 0, 0, 0);
    // FlushReplyBuffer(pContext, NULL, 0, NULL, 0);
    return Success;
}
static int _X_COLD SProcDebugProbeContext(ClientPtr client)
{
    REQUEST(xDebugProbeContextReq);

    swaps(&stuff->length);
    REQUEST_SIZE_MATCH(xDebugProbeContextReq);
    swapl(&stuff->context);
    return ProcDebugProbeContext(client);
}
#pragma endregion

// AddExtension function
static int ProcDebugDispatch(ClientPtr client)
{
    REQUEST(xReq);

    switch (stuff->data)
    {
    case X_DebugQueryVersion:
        return ProcDebugQueryVersion(client);
    case X_DebugCreateContext:
        return ProcDebugCreateContext(client);
    case X_DebugFreeContext:
        return ProcDebugFreeContext(client);
    case X_DebugRegisterClient:
        return ProcDebugRegisterClient(client);
    case X_DebugUnregisterClient:
        return ProcDebugUnregisterClient(client);
    case X_DebugProbeContext:
        return ProcDebugProbeContext(client);
    default:
        return BadRequest;
    }
    return 0;
}
static int _X_COLD SProcDebugDispatch(ClientPtr client)
{
    REQUEST(xReq);
    switch (stuff->data)
    {
    case X_DebugQueryVersion:
        return SProcDebugQueryVersion(client);
    case X_DebugCreateContext:
        return SProcDebugCreateContext(client);
    case X_DebugFreeContext:
        return SProcDebugFreeContext(client);
    case X_DebugRegisterClient:
        return SProcDebugRegisterClient(client);
    case X_DebugUnregisterClient:
        return SProcDebugUnregisterClient(client);
    case X_DebugProbeContext:
        return SProcDebugProbeContext(client);
    default:
        return BadRequest;
    }
    return 0;
}
static void DebugAClientStateChange(CallbackListPtr *pcbl, void *nulldata, void *calldata)
{
    NewClientInfoRec *pci = (NewClientInfoRec *)calldata;
    ClientPtr pClient = pci->client;

    switch (pClient->clientState)
    {
    case ClientStateRunning: /* new client */
        break;
    case ClientStateGone:
    case ClientStateRetained:
        FreeContextAsClient(pClient);

        break;

    default:
        break;
    }
}
static int DebugDeleteContext(void *value, XID id)
{
    DebugContextPtr pContext = (DebugContextPtr)value;

    if(LinkedListExistAsData(&allContexts, (char *)pContext)) 
        FreeContext(pContext);


    return Success;
}
static void DebugCloseDown(ExtensionEntry *extEntry)
{
    DeleteCallback(&ClientStateCallback, DebugAClientStateChange, NULL);
}

void DebugExtensionInit(void)
{
    ExtensionEntry *extentry;

    UDebugContext = CreateNewResourceType(DebugDeleteContext, "UDebugContext");
    if (!UDebugContext)
        return;

    // 用于拦截 request
    if (!dixRegisterPrivateKey(LogClientPrivateKey, PRIVATE_CLIENT, 0))
         return;

    // ppAllContexts = NULL;
    // numContexts = numEnabledContexts = numEnabledRCAPs = 0;

    LinkedListInit(&allContexts); //初始化这个链表

    if (!AddCallback(&ClientStateCallback, DebugAClientStateChange, NULL))
        return;

    extentry = AddExtension(DEBUG_NAME, DebugNumEvents, DebugNumErrors,
                            ProcDebugDispatch, SProcDebugDispatch,
                            DebugCloseDown, StandardMinorOpcode);
    if (!extentry)
    {
        DeleteCallback(&ClientStateCallback, DebugAClientStateChange, NULL);

        return;
    }
}
